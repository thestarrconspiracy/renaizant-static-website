require('dotenv').config({ silent: true });
const htmlStandards = require('reshape-standard');
const cssStandards = require('spike-css-standards');
const jsStandards = require('spike-js-standards');
const Contentful = require('spike-contentful');
const marked = require('marked');
const globImporter = require('node-sass-glob-importer');

const env = process.env.SPIKE_ENV;
const branch = process.env.BRANCH || 'master';
const previewToken = process.env.PREVIEW_TOKEN;
const deliveryToken = process.env.ACCESS_TOKEN;
const space = process.env.SPACE_ID;

// This is the local variable object to be accessed via template files
// The variables above implement dotenv and pull access tokens from an external source.
const locals = {};
// Add markdown to local variables so they can be used in templates
marked.setOptions({
  gfm: true,
  tables: true
});
locals.md = marked;
// Add current year to local variables
locals.year = new Date().getFullYear();
// Sitename to be used in templates
locals.sitename = 'Renaizant';
locals.env = env !== 'production' ? 'development' : 'production';

module.exports = {
  devtool: 'source-map',
  matchers: { html: '*(**/)*.html', css: '*(**/)*.scss' },
  ignore: [
    '**/index.html',
    'src/components/**/*.scss',
    'src/components/**/*.html',
    '**/.*',
    'readme.md',
    'yarn.lock'
  ],
  module: {
    rules: [
      {
        test: /\.scss/,
        use: [
          {
            loader: 'sass-loader',
            options: {
              importer: globImporter()
            }
          }
        ]
      },
      {
        test: /\.js/,
        use: [
          {
            loader: 'import-glob'
          }
        ]
      }
    ]
  },
  dumpDirs: ['src'],
  postcss: cssStandards({
    minify: env === 'production',
    warnForDuplicates: env !== 'production',
    plugins: { browsers: ['last 4 versions', '> 1%', 'ie >= 9'] }
  }),
  babel: jsStandards(),
  plugins: [
    new Contentful({
      addDataTo: locals,
      spaceId: space,
      accessToken: env !== 'production' ? previewToken : deliveryToken,
      preview: env !== 'production',
      environment: branch,
      // By default, this plugin will only fetch data once when you start your watcher, for development speed purposes. This means that if you change your data, you will have to restart the watcher to pick up the changes. If you are in a phase where you are making frequent data changes and would like a more aggressive updating strategy, you can set the aggressiveRefresh option to true, and your dreams will come true. However, note that this will slow down your local development, as it will fetch and link all entires every time you save a file, so it's only recommended for temporary use.
      // aggressiveRefresh: true,
      contentTypes: [
        {
          name: 'pages',
          id: 'page',
          template: {
            path: 'src/layout/page.html',
            output: page => {
              if (page.fields.slug === 'home') {
                return `index.html`;
              }
              return `${page.fields.slug}.html`;
            }
          }
        },
        {
          name: 'menus',
          id: 'menu'
        },
        {
          name: 'menuLinks',
          id: 'menuLink'
        }
        // {
        //   name: 'ctas',
        //   id: 'cta'
        // },
        // {
        //   name: 'grids',
        //   id: 'grid'
        // },
        // {
        //   name: 'press',
        //   id: 'press',
        //   filters: {
        //     order: '-fields.date'
        //   },
        //   transform: press => {
        //     const transformedPress = press;
        //     const summary = transformedPress.fields.text
        //       ? transformedPress.fields.text.substring(0, 150)
        //       : '';
        //     transformedPress.fields.date = moment(
        //       transformedPress.fields.date
        //     ).format('MMMM Do, YYYY');
        //     transformedPress.fields.summary = summary;
        //     transformedPress.fields.summary += '...';
        //     return transformedPress;
        //   },
        //   template: {
        //     path: 'src/layout/press.html',
        //     output: press => `press/${press.fields.slug}.html`
        //   }
        // }
      ],
      json: 'data.json'
    })
  ],
  reshape: htmlStandards({
    locals: () => locals,
    minify: env === 'production' ? { minifySvg: false } : false,
    retext: []
  }),
  entry: { 'assets/main': ['./src/assets/main.js'] }
};

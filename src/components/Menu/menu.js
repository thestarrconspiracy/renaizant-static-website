const hamburger = document.getElementsByClassName('mobile-icon');
const nav = document.getElementsByTagName('nav');

// Function to toggle active class to mobile menu
function classToggle() {
  this.classList.toggle('active');
  nav[0].classList.toggle('active');
}

hamburger[0].addEventListener('click', classToggle);

const h = document.getElementsByTagName('header');
let stuck = false;

function getDistance() {
  const topDist = h[0].offsetTop;
  return topDist;
}

const stickPoint = getDistance();

window.addEventListener('scroll', () => {
  const distance = getDistance() - window.pageYOffset + 20;
  const offset = window.pageYOffset;

  if (distance <= 0 && !stuck) {
    h[0].classList.add('scrolled');
    stuck = true;
  } else if (stuck && offset <= stickPoint) {
    h[0].classList.remove('scrolled');
    stuck = false;
  }
});

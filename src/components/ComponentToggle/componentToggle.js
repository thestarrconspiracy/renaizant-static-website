const componentToggleElement = document.getElementsByClassName(
  'component-toggle'
);

function componentToggle() {
  document.body.classList.toggle('show-components');
}

if (componentToggleElement.length) {
  componentToggleElement[0].addEventListener('click', componentToggle);
}

// document.body.classList.toggle('show-components');

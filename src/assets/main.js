/* eslint-disable */
import './polyfills.js';
import 'nodelist-foreach-polyfill'; // Polyfill forEach on nodeLists
import '../../node_modules/**/noframework.waypoints.min.js';
import './modernizr.js';
// import './magnify.js';
import '../components/**/*.js';
/* eslint-enable */

const sections = document.querySelectorAll('section');
[].map.call(sections, element => {
  /* eslint-disable */
  new Waypoint({
    element,
    handler() {
      element.classList.add('viewed');
    },
    offset: 'bottom-in-view'
  });
  /* eslint-enable */
});

function magnify(imgID, zoom) {
  let img, glass, w, h, bw;
  img = document.getElementById(imgID);

  /* create magnifier glass: */
  glass = document.createElement('DIV');
  glass.setAttribute('class', 'img-magnifier-glass');
  /* insert magnifier glass: */
  img.parentElement.insertBefore(glass, img);
  /* set background properties for the magnifier glass: */
  glass.style.backgroundImage = `url('${img.src}')`;
  glass.style.backgroundRepeat = 'no-repeat';
  glass.style.backgroundSize = `${img.width * zoom}px ${img.height * zoom}px`;
  bw = 3;
  w = glass.offsetWidth / 2;
  h = glass.offsetHeight / 2;
  /* execute a function when someone moves the magnifier glass over the image: */
  glass.addEventListener('mousemove', moveMagnifier);
  img.addEventListener('mousemove', moveMagnifier);
  /* and also for touch screens: */
  glass.addEventListener('touchmove', moveMagnifier);
  img.addEventListener('touchmove', moveMagnifier);

  img.addEventListener('click', showHide);

  function showHide(e) {
    console.log('click');
    glass.classList.add('show');
  }

  function moveMagnifier(e) {
    let pos, x, y;
    /* prevent any other actions that may occur when moving over the image */
    e.preventDefault();
    /* get the cursor's x and y positions: */
    pos = getCursorPos(e);
    x = pos.x;
    y = pos.y;
    /* prevent the magnifier glass from being positioned outside the image: */
    if (x > img.width - w / zoom) {
      x = img.width - w / zoom;
    }
    if (x < w / zoom) {
      x = w / zoom;
    }
    if (y > img.height - h / zoom) {
      y = img.height - h / zoom;
    }
    if (y < h / zoom) {
      y = h / zoom;
    }
    /* set the position of the magnifier glass: */
    glass.style.left = `${x - w}px`;
    glass.style.top = `${y - h}px`;
    /* display what the magnifier glass "sees": */
    glass.style.backgroundPosition = `-${x * zoom - w + bw}px -${y * zoom -
      h +
      bw}px`;
  }
  function getCursorPos(e) {
    let a,
      x = 0,
      y = 0;
    e = e || window.event;
    /* get the x and y positions of the image: */
    a = img.getBoundingClientRect();
    /* calculate the cursor's x and y coordinates, relative to the image: */
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /* consider any page scrolling: */
    x -= window.pageXOffset;
    y -= window.pageYOffset;
    return { x, y };
  }
}

/* Initiate Magnify Function with the id of the image, and the strength of the magnifier glass: */
/* eslint-disable */
// magnify('magnify-DhjJcJZziKeagO4gcs0E6', 2);
magnify('magnify-7j43VRfg3uoSSoCUka2aYK', 2);
/* eslint-enable */
